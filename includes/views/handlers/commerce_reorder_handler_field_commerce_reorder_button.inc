<?php
/**
 *
 */

class commerce_reorder_handler_field_commerce_reorder_button extends views_handler_field_entity {

  function option_definition() {
    $options = parent::option_definition();
    $options['reorder_button_label'] = array('default' => 'Reorder', 'translatable' => TRUE);
    $options['redirect'] = array('default' => TRUE, 'bool' => TRUE);
    $options['redirect_url'] = array('default' => 'cart');
    $options['suppress_cart_messages'] = array('default' => TRUE, 'bool' => TRUE);
    $profile_types = array_keys(commerce_customer_profile_types());
    foreach ($profile_types as $type) {
      $options['copy_profiles'][$type] = array('default' => FALSE, 'bool' => TRUE);
    }
    $options['copy_profiles_source'] = array('default' => 'order');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['reorder_button_label'] = array(
      '#type' => 'textfield',
      '#title' => t('Reorder button label'),
      '#default_value' => $this->options['reorder_button_label'],
    );
    $form['redirect'] = array(
      '#type' => 'checkbox',
      '#title' => t('Redirect after reorder'),
      '#default_value' => $this->options['redirect'],

    );
    $form['redirect_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Target URL to redirect after reorder'),
      '#default_value' => $this->options['redirect_url'],
      '#description' => t('Tokens are supported. For instance you may use <strong></strong><code>checkout/[site:current-cart-order:order-id]/review</code></strong> to go directly to the review step.'),
      '#dependency' => array('edit-options-redirect' => array(TRUE)),
    );
    $types = commerce_customer_profile_types();
    $copy_profile_options = array();
    foreach ($types as $type => $type_data) {
      $copy_profile_options[$type] = t($type_data['name']);
    }
    $form['suppress_cart_messages'] = array(
      '#type' => 'checkbox',
      '#title' => t('Suppress add to cart messages'),
      '#default_value' => $this->options['suppress_cart_messages'],
      '#description' => t('Enable this to hide any status message updates that occur during the order copy. Will still allow errors and warnings to come through.'),
    );
    $form['copy_profiles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Copy customer profiles to the new order'),
      '#description' => t('The selected profiles will be copied to the new order.'),
      '#options' => $copy_profile_options,
      '#default_value' => $this->options['copy_profiles'],
    );
    $source_options = array('order' => t('Original order'));
    if (module_exists('commerce_addressbook')) {
      $source_options['addressbook'] = t('Address book defaults');
    }
    $form['copy_profiles_source'] = array(
      '#title' => t('Copy customer profile information from'),
      '#type' => 'radios',
      '#default_value' => $this->options['copy_profiles_source'],
      '#options' => $source_options,
      '#states' => array(
        'visible' => array(
          ':input[name*="[copy_profiles]"]' => array('checked' => TRUE),
        ),
      ),
      '#description' => t('Profiles can be copied from the original order or from the address book if available.'),
    );
  }

  function construct() {
    parent::construct();
    $this->additional_fields['order_id'] = 'order_id';
    $this->real_field = 'order_id';
  }

  function render($values) {
    $order = $this->get_value($values);
    if (!empty($order)) {
      $settings = array(
        'copy_profiles' => $this->options['copy_profiles'],
        'copy_profiles_source' => $this->options['copy_profiles_source'],
        'suppress_cart_messages' => $this->options['suppress_cart_messages'],
      );
      $form = drupal_get_form('commerce_reorder_reorder_' . $order->order_id, $order, $settings, $this->options);
      return drupal_render($form);
    }
  }
}
